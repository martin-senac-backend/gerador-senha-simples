<?php
session_start();
?>

<!DOCTYPE html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Gerador de senha</title>

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@400;700&display=swap" rel="stylesheet">

    <style>
        * {
            box-sizing: border-box;
        }

        body {
            box-sizing: border-box;
            overflow: hidden;
        }

        .container {
            display: flex;
            justify-content: center;
            align-items: center;
            height: 100vh;
        }

        #geral {
            width: 400px;
            border: 1px solid #ccc;
            PADDING: 20px;
            border-radius: px;
            box-shadow: 0px 0px 30px #ccc;

        }

        .btn {
            width: 100%;
            padding: 10px;
            border: none;
            font-size: 1.2rem;
        }

        .btn-azul {
            background-color: #054F77;
        }

        form input {
            width: 100%;
            padding: 10px;
            font-size: 1rem;
            border-radius: 5px;
            border: 1px solid #ccc;
        }

        .alerta {
            padding: 10px;
            background: #ffe6e6;
            color: #ff0000;
        }
    </style>

</head>

<body>
    <div class="container">

        <div id="geral">

            <?php if (isset($_SESSION['alerta'])) : ?>

                <div class="alerta">
                    <?= $_SESSION['alerta'] ?>
                </div>

            <?php
                unset($_SESSION['alerta']);
            endif;

            ?>

            <header>

                <h1>Gerador de senha</h1>

            </header>

            <main>

                <form action="gerar.php" method="GET">

                    <p>
                        <label for="limite">Limite de Senha: </label><br>
                        <input type="text" name="limite" id="limite">
                    </p>

                    <p>
                        <button type="submit" class="btn btn-azul">Gerar Senha</button>
                    </p>

                </form>

            </main>

            <footer>

                <p>
                    Copyright - 2022 | Desenvolvido por Martin de Almeida
                </p>

            </footer>

        </div>


</body>

</html>